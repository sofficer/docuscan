﻿// Learn more about F# at http://fsharp.net

module Scanner
open System.Drawing
open System.IO

let ``open image`` path width =
    use image = Bitmap.FromFile(path)
    let aspect_ratio = image.Height / image.Width
    new Bitmap(image, width, (int)(image.Height * aspect_ratio))

let ``to pixel map`` (image : Bitmap) =
    let ``to grayscale`` (source_pixel : Color) = 
        let result = ((float)source_pixel.R * 0.21) + ((float)source_pixel.G * 0.72) + ((float)source_pixel.B * 0.07)
        (int)result

    let ``pixel filter`` source_pixel = source_pixel < 90

    let width = image.Width
    let height = image.Height

    let pixel_map = Array.create (width * height) false

    for row in 0..image.Height-1 do
        for column in 0..image.Width-1 do
            let pixel = image.GetPixel(column, row) |> ``to grayscale`` |> ``pixel filter``
            let index = (row * width) + column
            pixel_map.[index] <- pixel
    pixel_map

let pmap = ``open image`` @"D:\test.png" 321 |> ``to pixel map``

let out_stream = new StreamWriter(@"D:\output_image.txt")

let ``save map`` index pixel = 
    if (index <> 0 && index % 321 = 0) then out_stream.WriteLine("")
    out_stream.Write(if pixel then "#" else " ")
    ()

Array.iteri ``save map`` pmap
out_stream.Dispose()
