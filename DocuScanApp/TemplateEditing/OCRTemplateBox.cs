﻿using System.Drawing;
using System.Windows;

namespace DocuScanApp
{
    public class OCRTemplateBox : TemplateBox
    {
        public OCRTemplateBox(string cell_ref, Rect bounds) : base(cell_ref, bounds) {
            
        }

        //----------------------------------------------------------------------------------------------------
                
        public string GetString(Bitmap source) {
            return "";
        }

        //----------------------------------------------------------------------------------------------------
                
        public override TemplateBox WithBounds(Rect new_bounds) {
            return new OCRTemplateBox(ExcelCell, new_bounds);
        }
    }
}
