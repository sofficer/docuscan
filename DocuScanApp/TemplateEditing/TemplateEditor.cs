﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using DocuScanApp.TemplateEditing;

namespace DocuScanApp
{
    class TemplateEditor : Adorner {
        private bool m_is_dropping = false;
        
        private static readonly Pen m_box_pen;
        private static readonly Pen m_moving_box_pen;

        private readonly Template m_template;
        private readonly TransformManager m_transform_manager;
        

        private Point m_mouse_down_point;
        private Point m_current_point;
        private TemplateBox m_selected_box;
        private int m_counter = 0;

        //----------------------------------------------------------------------------------------------------
        
        static TemplateEditor() {
            m_box_pen = new Pen(Brushes.Red, 1.0);
            m_box_pen.Freeze();

            m_moving_box_pen = new Pen(Brushes.LightCoral, 1.0) { DashStyle = DashStyles.Dash };
            m_moving_box_pen.Freeze();
        }

        //----------------------------------------------------------------------------------------------------
                
        public TemplateEditor(
            TransformManager transform_manager, Template template, Image adorned_element
        ) : base(adorned_element) {
            AdornedImage = adorned_element;
            m_template = template;
            m_transform_manager = transform_manager;
        }

        //----------------------------------------------------------------------------------------------------

        private Image AdornedImage { get; set; }

        //----------------------------------------------------------------------------------------------------
        
        protected override void OnMouseDown(MouseButtonEventArgs e) {
            var position = e.GetPosition(AdornedElement);
            var document_position = m_transform_manager.TransformPointToDocumentSpace(position);
            var clicked_box = FindClickedBox(document_position);
            var click_on_existing_box = clicked_box != null;

            if (e.LeftButton == MouseButtonState.Pressed) {
                // Check if we are clicking on an existing box.
                m_mouse_down_point = position;
                m_current_point = m_mouse_down_point;

                // If not then enter dropping mode
                if (!click_on_existing_box) {
                    m_is_dropping = true;
                    m_selected_box = null;
                } else {
                    // Else we are editing an existing box 
                    m_is_dropping = false;
                    m_selected_box = clicked_box;

                    // Should determine where on the box we are, so that the user can resize the box
                }    
            } else if (e.RightButton == MouseButtonState.Pressed && click_on_existing_box) {
                var template_box_editor = new TemplateBoxEditor();
                var result = template_box_editor.ShowDialog();
                if (result.HasValue && result.Value) {
                    // Check which type the user changed the box to
                    // They could also have changed which cell in the excel document gets populated
                    // Else, Check if the user wants to delete the control
                }
            }
        }

        //----------------------------------------------------------------------------------------------------

        private TemplateBox FindClickedBox(Point document_position) {
            var clicked_box = m_template.Boxes.FirstOrDefault(box => box.Bounds.Contains(document_position));
            return clicked_box;
        }

        //----------------------------------------------------------------------------------------------------

        protected override void OnMouseMove(MouseEventArgs e) {
            var position = e.GetPosition(AdornedElement);

            m_current_point = position;

            if (m_is_dropping || m_selected_box != null) {
                InvalidateVisual();    
            }
        }

        //----------------------------------------------------------------------------------------------------
                
        protected override void OnMouseUp(MouseButtonEventArgs e) {
            var position = e.GetPosition(AdornedElement);

            // Find out where the points relate to the actual source image
            var start_point = m_transform_manager.TransformPointToDocumentSpace(m_mouse_down_point);
            var end_point = m_transform_manager.TransformPointToDocumentSpace(position);

            // Exit whichever mode we are in
            if (m_is_dropping) {
                m_is_dropping = false;
                
                var document_bounds = new Rect(start_point, end_point);
                var box = PromptUserForBoxType(ToCellRef(m_counter++), document_bounds);

                m_template.AddBox(box);
            } else if (m_selected_box != null) {
                // Work out how much the user has dragged the box by
                var distance = Point.Subtract(end_point, start_point);
                var new_topleft = Point.Add(m_selected_box.Bounds.TopLeft, distance);
                var new_bottomright = Point.Add(m_selected_box.Bounds.BottomRight, distance);

                // Replace the box with a new box in the new position
                m_template.ReplaceBox(
                    m_selected_box, 
                    m_selected_box.WithBounds(new Rect(new_topleft, new_bottomright))
                );
                m_selected_box = null;
            }
            
            InvalidateVisual();
        }

        //----------------------------------------------------------------------------------------------------

        protected override void OnRender(DrawingContext drawingContext) {
            // Draw a transparent area so that we can receive mouse events because we are hit testable
            var control_area = new Rect(new Size(ActualWidth, ActualHeight));
            drawingContext.DrawRectangle(Brushes.Transparent, null, control_area);

            // Need to map the boxes into view space from document space
            // Also, don't draw the box if it is the selected one because we will draw it later on in its
            //  temporary position.
            var drawable_boxes = m_template
                .Boxes
                .Where(box => box != m_selected_box)
                .Select(box => new { Text = box.ExcelCell, Bounds = m_transform_manager.TransformToViewBounds(box.Bounds)});
            foreach (var box in drawable_boxes) {
                drawingContext.DrawRectangle(null, m_box_pen, box.Bounds);
                drawingContext.DrawText(
                    new FormattedText(
                        box.Text, 
                        System.Threading.Thread.CurrentThread.CurrentUICulture,
                        FlowDirection.LeftToRight, 
                        new Typeface("Tahoma"), 
                        10.0,
                        Brushes.LightCoral
                    ), 
                    box.Bounds.TopLeft
                );
            }

            if (m_is_dropping) {
                // Draw the box that the user is busy creating
                drawingContext.DrawRectangle(null, m_box_pen, new Rect(m_mouse_down_point, m_current_point));
            } else if (m_selected_box != null) {
                // Draw the box that the user is busy dragging around
                var distance = Point.Subtract(m_current_point, m_mouse_down_point);
                var view_bounds = m_transform_manager.TransformToViewBounds(m_selected_box.Bounds);
                var new_topleft = Point.Add(view_bounds.TopLeft, distance);
                var new_bottomright = Point.Add(view_bounds.BottomRight, distance);
                drawingContext.DrawRectangle(null, m_moving_box_pen, new Rect(new_topleft, new_bottomright));
            }
        }

        //----------------------------------------------------------------------------------------------------
        
        private static TemplateBox PromptUserForBoxType(string cell_ref, Rect bounds) {
            // Show a dialog that allows the user to specify how that area of the document should be 
            // interpreted (using OCR or mark detection)
            return new OCRTemplateBox(cell_ref, bounds);
        }

        //----------------------------------------------------------------------------------------------------
        
        private static string ToCellRef(int counter) {
            var nums = new[] {
                "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "X", "W", "Y", "Z"
            };
            var nbase = nums.Length;

            var highest_power = (int)(Math.Log(counter, nbase));
            var result = string.Empty;
            var remainder = counter;

            for (var i = highest_power; i >= 0; i--) {
                var value = (int)Math.Pow(nbase, i);
                if (value > remainder) {
                    result += nums[0];
                } else {
                    int rem;
                    var div = Math.DivRem(remainder, value, out rem);
                    result += nums[div];
                    remainder -= (div * value);
                }
            }

            return result;
        }
    }
}
