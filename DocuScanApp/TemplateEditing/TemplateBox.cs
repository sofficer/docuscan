﻿using System.IO;
using System.Windows;

namespace DocuScanApp
{
    public abstract class TemplateBox
    {
        protected TemplateBox(string cell_ref, Rect bounds) {
            ExcelCell = cell_ref;
            Bounds = bounds;
        }

        //----------------------------------------------------------------------------------------------------

        public Rect Bounds { get; private set; }

        //----------------------------------------------------------------------------------------------------

        public string ExcelCell { get; private set; }

        //----------------------------------------------------------------------------------------------------
        
        public void Serialize(StreamWriter stream_writer) {
            stream_writer.WriteLine(GetType().Name);
            stream_writer.WriteLine(Bounds);
            stream_writer.WriteLine(ExcelCell);
        }

        //----------------------------------------------------------------------------------------------------

        public abstract TemplateBox WithBounds(Rect new_bounds);
    }
}
