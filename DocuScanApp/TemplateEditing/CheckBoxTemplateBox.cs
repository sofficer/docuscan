﻿using System.Drawing;
using System.Windows;

namespace DocuScanApp
{
    // Represents an area of the document that can either contain a mark or can be empty
    public class CheckBoxTemplateBox : TemplateBox
    {
        public CheckBoxTemplateBox(string cell_ref, Rect bounds) : base(cell_ref, bounds) {
            
        }

        //----------------------------------------------------------------------------------------------------
                
        public bool ContainsMark(Bitmap source) {
            return true;
        }

        //----------------------------------------------------------------------------------------------------
                
        public override TemplateBox WithBounds(Rect new_bounds) {
            return new CheckBoxTemplateBox(ExcelCell, new_bounds);
        }
    }
}
