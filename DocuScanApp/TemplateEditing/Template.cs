﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;

namespace DocuScanApp.TemplateEditing
{
    public class Template {
        private readonly  IList<TemplateBox> m_boxes;
        private readonly IEnumerable<TemplateBox> m_readonly_boxes;


        //----------------------------------------------------------------------------------------------------
        
        public Template() {
            m_boxes = new List<TemplateBox>();
            m_readonly_boxes = new ReadOnlyCollection<TemplateBox>(m_boxes);
        }

        //----------------------------------------------------------------------------------------------------
                
        public void AddBox(TemplateBox box) {
            m_boxes.Add(box);
        }

        //----------------------------------------------------------------------------------------------------

        public void ReplaceBox(TemplateBox current_box, TemplateBox new_box) {
            var index = m_boxes.IndexOf(current_box);
            m_boxes[index] = new_box;
        }

        //----------------------------------------------------------------------------------------------------
        
        public IEnumerable<TemplateBox> Boxes {
            get { return m_readonly_boxes; }
        }

        //----------------------------------------------------------------------------------------------------
        
        public void Save(string path) {
            using (var stream_writer = new StreamWriter(path)) {
                stream_writer.WriteLine(m_boxes.Count);
                foreach (var box in m_boxes) {
                    box.Serialize(stream_writer);
                }    
            }
        }
    }
}
