﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Point = System.Windows.Point;

namespace DocuScanApp
{
    public class TransformManager {
        private readonly Image m_display;
        private readonly ImageSource m_source;

        public TransformManager(Image image) {
            m_display = image;
            m_source = image.Source;
        }

        public Point TransformPointToDocumentSpace(Point view_point) {
            var x_scaling = m_source.Width / m_display.ActualWidth;
            var y_scaling = m_source.Height / m_display.ActualHeight;

            return new Point(view_point.X * x_scaling, view_point.Y * y_scaling);
        }

        //----------------------------------------------------------------------------------------------------
                
        public Point TransformPointToViewSpace(Point document_point) {
            var x_scaling = m_display.ActualWidth / m_source.Width;
            var y_scaling = m_display.ActualHeight / m_source.Height;

            return new Point(document_point.X * x_scaling, document_point.Y * y_scaling);
        }

        //----------------------------------------------------------------------------------------------------
        
        public Rect TransformToDocumentBounds(Rect view_rect) {
            var top_left = TransformPointToDocumentSpace(view_rect.TopLeft);
            var bottom_right = TransformPointToDocumentSpace(view_rect.BottomRight);

            return new Rect(top_left, bottom_right);
        }

        //----------------------------------------------------------------------------------------------------

        public Rect TransformToViewBounds(Rect document_rect) {
            var top_left = TransformPointToViewSpace(document_rect.TopLeft);
            var bottom_right = TransformPointToViewSpace(document_rect.BottomRight);

            return new Rect(top_left, bottom_right);
        }
    }
}
