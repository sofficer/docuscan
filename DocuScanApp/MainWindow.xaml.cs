﻿using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using DocuScanApp.TemplateEditing;

namespace DocuScanApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        private Template m_template;

        public MainWindow() {
            InitializeComponent();

            var region_editor = new TemplateEditor(
                new TransformManager(m_document), 
                m_template = new Template(), 
                m_document
            );
            var adorner_layer = AdornerLayer.GetAdornerLayer(m_document);
            adorner_layer.Add(region_editor);
        }

        private void SaveClick(object sender, RoutedEventArgs e) {
            m_template.Save(@"D:\mytemplate.txt");
        }
    }
}
